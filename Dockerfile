# Stage 0, based on Node.js, to build and compile Angular
FROM node:latest as node

# Change direcctory so that our commands run inside this new directory
WORKDIR /app

# Copy dependency definitions
COPY package.json /app/

# Install Dependencies

RUN npm install -g @angular/cli
RUN npm install

# Copy over project files
COPY ./ /app/

# Compile angular for prod
RUN npm run build --prod

# Stage 1, based on Nginx, to have only the compiled app, ready for production with Nginx
FROM nginx:1.13

#Copy dist files from node parent container into new nginx container
COPY --from=node /app/dist/iframe/ /usr/share/nginx/html

#Copy the custom conf which includes the proxy pass to the middleware
COPY ./nginx-custom.conf /etc/nginx/conf.d/default.conf
