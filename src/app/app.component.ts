import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  title = 'Arms Case 1';
  panelOpenState = false;

  myThumbnail = './assets/Breasts/Case-1/Breast-Case-1-Angle-Before.jpg';

  active = [ { url: '/assets/Arms/Case-1/Arm-Case-1-Before.jpg'},
  { url: '/assets/Arms/Case-1/Arm-Case-1-After.jpg'}];

  gallery = {
    arms: {
     case1: [
        { url: '/assets/Arms/Case-1/Arm-Case-1-Before.jpg'},
        { url: '/assets/Arms/Case-1/Arm-Case-1-After.jpg'}
      ]
  },
  breasts: {
    case1: [
      { url: '/assets/Breasts/Case-1/Breast-Case-1-Angle-Before.jpg'},
      { url: '/assets/Breasts/Case-1/Breast-Case-1-Angle-After.jpg'},
      { url: '/assets/Breasts/Case-1/Breast-Case-1-Straight-Before.jpg'},
      { url: '/assets/Breasts/Case-1/Breast-Case-1-Straight-After.jpg'}
    ],
    case2: [
      { url: '/assets/Breasts/Case-2/Breast-Case-2-Angle-Before.jpg'},
      { url: '/assets/Breasts/Case-2/Breast-Case-2-Angle-After.jpg'},
      { url: '/assets/Breasts/Case-2/Breast-Case-2-Front-Before.jpg'},
      { url: '/assets/Breasts/Case-2/Breast-Case-2-Front-After.jpg'},
      { url: '/assets/Breasts/Case-2/Breast-Case-2-Side-Before.jpg'},
      { url: '/assets/Breasts/Case-2/Breast-Case-2-Side-After.jpg'}
    ],
    case3: [
      { url: '/assets/Breasts/Case-6/Breast-Case-6-Angle-Before.jpg'},
      { url: '/assets/Breasts/Case-6/Breast-Case-6-Side-After.jpg'},
      { url: '/assets/Breasts/Case-6/Breast-Case-6-Side-Before.jpg'},
      { url: '/assets/Breasts/Case-6/Breast-Case-6-Straight-After.jpg'},
      { url: '/assets/Breasts/Case-6/Breast-Case-6-Straight-Before.jpg'}
    ],
    case4: [
      { url: '/assets/Breasts/Case-7/Breast-Case-7-Angle-Before.jpg'},
      { url: '/assets/Breasts/Case-7/Breast-Case-7-Angle-After.jpg'},
      { url: '/assets/Breasts/Case-7/Breast-Case-7-Straight-Before.jpg'},
      { url: '/assets/Breasts/Case-7/Breast-Case-7-Straight-After.jpg'}
    ],
    case5: [
      { url: '/assets/Breasts/Case-8/Breast-Case-8-Angle-Before.jpg'},
      { url: '/assets/Breasts/Case-8/Breast-Case-8-Angle-After.jpg'},
      { url: '/assets/Breasts/Case-8/Breast-Case-8-Straight-Before.jpg'},
      { url: '/assets/Breasts/Case-8/Breast-Case-8-Straight-After.jpg'},
      { url: '/assets/Breasts/Case-8/Breast-Case-8-Side-Before.jpg'},
      { url: '/assets/Breasts/Case-8/Breast-Case-8-Side-After.jpg'}
    ],
    case6: [
      { url: '/assets/Breasts/Case-10/Breast-Case-10-Straight-Before.jpg'},
      { url: '/assets/Breasts/Case-10/Breast-Case-10-Straight-After.jpg'}
    ],
    case7: [
      { url: '/assets/Breasts/Case-11/Breast-Case-11-Angle-Before.jpg'},
      { url: '/assets/Breasts/Case-11/Breast-Case-11-Angle-After.jpg'},
      { url: '/assets/Breasts/Case-11/Breast-Case-11-Straight-Before.jpg'},
      { url: '/assets/Breasts/Case-11/Breast-Case-11-Straight-After.jpg'},
      { url: '/assets/Breasts/Case-11/Breast-Case-11-Side-Before.jpg'},
      { url: '/assets/Breasts/Case-11/Breast-Case-11-Side-After.jpg'}
    ]
  },
  butt: {
    case1: [
      { url: '/assets/Butt/Case-1/Butt-Case-1-Before.jpg'},
      { url: '/assets/Butt/Case-1/Butt-Case-1-After.jpg'}
    ],
    case2: [
      { url: '/assets/Butt/Case-2/Butt-Case-2-Straight-Before.jpg'},
      { url: '/assets/Butt/Case-2/Butt-Case-2-Straight-After.jpg'}
    ]

     },
    faces: {
      case1: [
        { url: '/assets/Faces/Case-3/Face-Case-3-Angle-Before.png'},
        { url: '/assets/Faces/Case-3/Face-Case-3-Angle-After.png'}
      ],
      case2: [
        { url: '/assets/Faces/Case-2/Face-Case-2-Before.jpg'},
        { url: '/assets/Faces/Case-2/Face-Case-2-After.jpg'}
      ]
       },
    lipo: {
        case1: [
          { url: '/assets/Lipo/Case-1/Lipo-Case-1-Straight-Before.png'},
          { url: '/assets/Lipo/Case-1/Lipo-Case-1-Straight-After.png'}
        ]
         },
    mommy: {
          case1: [
            { url: '/assets/Mommy-Makeover/Case-1/MommyMakeover-Case-1-SideBend-Before.jpg'},
            { url: '/assets/Mommy-Makeover/Case-1/MommyMakeover-Case-1-SideBend-After.jpg'}
          ],
          case2: [
            { url: '/assets/Mommy-Makeover/Case-2/MommyMakeover-Case-2-SideBend-Before.jpg'},
            { url: '/assets/Mommy-Makeover/Case-2/MommyMakeover-Case-2-SideBend-After.jpg'}
          ]
           },
           rhinoplasty: {
            case1: [
              { url: '/assets/Rhinoplasty/Case-1/Rhinoplasty-Case-1-Angle-Before.jpg'},
              { url: '/assets/Rhinoplasty/Case-1/Rhinoplasty-Case-1-Angle-After.jpg'}
            ],
            case2: [
              { url: '/assets/Rhinoplasty/Case-2/Rhinoplasty-Case-2-Angle-Before.jpg'},
              { url: '/assets/Rhinoplasty/Case-2/Rhinoplasty-Case-2-Angle-After.jpg'}
            ]
             }
  };

  contentArray = new Array(90).fill('');
  returnedArray: any;
  showDirectionLinks = true;


  ngOnInit(): void {

    this.returnedArray = this.active;
  }

  refreshGallery(): void {
    this.pageChanged({itemsPerPage: 2, page: 1});
    return;
  }
  pageChanged(event): void {
    console.log('hit', event);
    const startItem = (event.page - 1) * event.itemsPerPage;
    const endItem = event.page * event.itemsPerPage;

    this.returnedArray = this.active.slice(startItem, endItem);
  }

  changeActive(type) {
console.log('hit');
    switch (type) {
      case 'arms1': {
        this.active = this.gallery.arms.case1;
        this.title = 'Arms Case 1';
        this.refreshGallery();
        break;
      }
      case 'breasts1': {
        this.active = this.gallery.breasts.case1;
        this.title = 'Breasts Case 1';
        this.refreshGallery();
        break;
      }
      case 'breasts2': {
        this.active = this.gallery.breasts.case2;
        this.title = 'Breasts Case 2';
        this.refreshGallery();
        break;
      }
      case 'breasts3': {
        this.active = this.gallery.breasts.case3;
        this.title = 'Breasts Case 3';
        this.refreshGallery();
        break;
      }
      case 'breasts4': {
        this.active = this.gallery.breasts.case4;
        this.title = 'Breasts Case 4';
        this.refreshGallery();
        break;
      }
      case 'breasts5': {
        this.active = this.gallery.breasts.case5;
        this.title = 'Breasts Case 5';
        this.refreshGallery();
        break;
      }
      case 'breasts6': {
        this.active = this.gallery.breasts.case6;
        this.title = 'Breasts Case 6';
        this.refreshGallery();
        break;
      }
      case 'breasts7': {
        this.active = this.gallery.breasts.case7;
        this.title = 'Breasts Case 7';
        this.refreshGallery();
        break;
      }
      case 'butt1': {
        this.active = this.gallery.butt.case1;
        this.title = 'Butt Case 1';
        this.refreshGallery();
        break;
      }
      case 'butt2': {
        this.active = this.gallery.butt.case2;
        this.title = 'Butt Case 2';
        this.refreshGallery();
        break;
      }
      case 'face1': {
        this.active = this.gallery.faces.case1;
        this.title = 'Face Case 1';
        this.refreshGallery();
        break;
      }
      case 'face2': {
        this.active = this.gallery.faces.case2;
        this.title = 'Face Case 1';
        this.refreshGallery();
        break;
      }
      case 'lipo1': {
        this.active = this.gallery.lipo.case1;
        this.title = 'Face Case 3';
        this.refreshGallery();
        break;
      }
      case 'mommy1': {
        this.active = this.gallery.mommy.case1;
        this.title = 'Mommy Makeover Case 1';
        this.refreshGallery();
        break;
      }
      case 'mommy2': {
        this.active = this.gallery.mommy.case2;
        this.title = 'Mommy Makeover Case 2';
        this.refreshGallery();
        break;
      }
      case 'rhinoplasty1': {
        this.active = this.gallery.rhinoplasty.case1;
        this.title = 'rhinoplasty Case 1';
        this.refreshGallery();
        break;
      }
      case 'rhinoplasty2': {
        this.active = this.gallery.rhinoplasty.case2;
        this.title = 'rhinoplasty Case 2';
        this.refreshGallery();
        break;
      }
  }

  console.log(this.active);
}
}
